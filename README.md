# LFS-Debian11Host



## Host System Requirements

``` apt update && apt install curl binutils bison gawk gcc g++ m4 make patch texinfo dosfstools gettext -y ```

### Check Host System Requirements

``` sh -c "$(curl -fsSL https://gitlab.com/A.Alperen.YLDM/lfs-debian11host/-/raw/main/checkhostsystemrequirements.sh)" ```

## Creating a limited directory

``` sh -c "$(curl -fsSL https://gitlab.com/A.Alperen.YLDM/lfs-debian11host/-/raw/main/creatingalimiteddirectory.sh)" ```

### Creating LFS User

``` sh -c "$(curl -fsSL https://gitlab.com/A.Alperen.YLDM/lfs-debian11host/-/raw/main/creatinglfsuser.sh)" ```

## Cross Compile Chain Tools Compile

**IMPORTANT!!!** Make sure the $LFS variable is set ``` echo $LFS ```

``` cd $LFS/sources && sh -c "$(curl -fsSL https://gitlab.com/A.Alperen.YLDM/lfs-debian11host/-/raw/main/crosscompiletoolchain.sh)" ```
